'use strict';

var Config = {
	server: {
		adress: 'localhost',
		port: 80
	}, 
	db: {
		adress: '',
		port: '',
		name: ''
	}
};

module.exports = Config;