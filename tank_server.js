'use strict';

const config = require('./config.js');
const express = require('express');
const app = express();
const server = app.listen(config.server.port, function() {
	console.log('Server listening at' + ' http://'
				+ config.server.adress + ':' + config.server.port);
});
const io = require('socket.io').listen(server);
const path = require('path');

app.use(express.static(path.join(__dirname+'/client')));

app.get('/', function(req, res) {
	res.sendFile(__dirname+'/client/index.html');
});

// Array of users
// Socket ID will be just identificator, HA-HA
var users = {};

// User object
function User() {
	// User tank coordinates
	this.x = 100;
	this.y = 100;
	// System information
	this.nickname = 'Anonymous';
	// Command stack
	this.directionStack = [];
}

User.prototype = {
	type : "User",

	handleCommand : function(command) {
		switch(command) {
			// Сделать какую-то проверку для тех, кто захочет положить сервер бесконечно много эмулируя
			// только нажатия клавиши без отпускания
			case 'pushUp':
				this.directionStack.unshift('up');
				break;
			case 'pushDown':
				this.directionStack.unshift('down');
				break;
			case 'pushLeft':
				this.directionStack.unshift('left');
				break;
			case 'pushRight':
				this.directionStack.unshift('right');
				break;
			case 'releaseUp':
				for(var i = 0; i < this.directionStack.length; i++) {
					if(this.directionStack[i] == 'up') {
						this.directionStack.splice(i, 1);
					}
				}
				break;
			case 'releaseDown':
				for(var i = 0; i < this.directionStack.length; i++) {
					if(this.directionStack[i] == 'down') {
						this.directionStack.splice(i, 1);
					}
				}
				break;
			case 'releaseLeft':
				for(var i = 0; i < this.directionStack.length; i++) {
					if(this.directionStack[i] == 'left') {
						this.directionStack.splice(i, 1);
					}
				}
				break;
			case 'releaseRight':
				for(var i = 0; i < this.directionStack.length; i++) {
					if(this.directionStack[i] == 'right') {
						this.directionStack.splice(i, 1);
					}
				}
				break;
			case 'pushShoot':
				this.shoot();
				break;
			default:
				break;
		}
	},

	move : function() {
		switch(this.directionStack[0]) {
			case 'up':
				this.y -= 2;
				break;
			case 'down':
				this.y += 2;
				break;
			case 'left':
				this.x -= 2;
				break;
			case 'right':
				this.x += 2;
				break;
			default:
				break;
		}
	},

	shoot : function() {

	}
};

io.on('connection', function(socket) {

	console.log('New user with id ' + socket.id + ' connected');
	users[socket.id] = new User();

	socket.on('name', function(name) {
		name = JSON.parce(name);
		users[socket.id].nickname = name;
	});

	socket.on('command', function(command) {
		console.log('User ' + socket.id + ' sent command ' + command);
		users[socket.id].handleCommand(command);
	});
	
	socket.on('disconnect', function() {
		console.log('User with id ' + socket.id + ' disconnected');
		delete users[socket.id];
	});

});

var mainLoopID = setInterval(function() {
	
	io.emit('demandCommand');

	for(var key in users) {
		users[key].move();
	}

	io.emit('update', JSON.stringify(users));
}, 50);