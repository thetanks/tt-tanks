var KEYBOARD = {
        87: 'Up',           // up - W
        65: 'Left',         // left - A
        83: 'Down',         // down - S
        68: 'Right',        // right - D
        32: 'Shot'          // shot - spacebar
    };


var buttons = Object.create(null);
for(var key in KEYBOARD) {
	buttons[key] = false;
}

var lastKey;
var commandStack = [];

document.addEventListener('keydown', function(event) {
	var keyState = buttons[event.keyCode];

	if(keyState !== undefined && keyState != true) {
		buttons[event.keyCode] = true;
		var key = KEYBOARD[event.keyCode];
		commandStack.unshift('push'+ key);
		lastKey = key;
		console.log(commandStack);
	}
});

document.addEventListener('keyup', function(event) {
	if(buttons[event.keyCode] !== undefined) {
		buttons[event.keyCode] = false;
		var key = KEYBOARD[event.keyCode];
		commandStack.unshift('release'+ key);

		console.log(commandStack);
	}
});

function getMovementDirection() {
	return lastKey;
}

function getInputCommand() {
	return commandStack.shift();
}
