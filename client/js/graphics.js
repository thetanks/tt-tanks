var renderer = PIXI.autoDetectRenderer(800, 600,{backgroundColor : 0x1099bb});
document.getElementById("map").appendChild(renderer.view);

var stage = new PIXI.Container();

var texture = PIXI.Texture.fromImage('../content/tank.png');

// create a new Sprite using the texture
var tank = new PIXI.Sprite(texture);

// center the sprite's anchor point
tank.anchor.x = 0.5;
tank.anchor.y = 0.5;

stage.addChild(tank);

// start animating
animate();
var rot;
function animate() {
    requestAnimationFrame(animate);
    renderer.render(stage);
}

function getRotation(xPos, yPos) {
	var rotations = {
		"Up": -90,
		"Down": 90,
		"Left": 180,
		"Right": 0
	};
	var rot;
	var dX = tank.position.x - xPos;
	var dY = tank.position.y - yPos;

	if(dX > 0) {
		rot = 0;
	} else {
		rot = 180;
	} 
	if(dY > 0) {
		rot = 90;
	} else {
		rot = -90;
	}
	return rot;
}

function setPosition(xPos, yPos) {
	tank.rotation  = radians(getRotation(xPos, yPos));

	console.log(getRotation(xPos, yPos));
	tank.position.x = xPos;
	tank.position.y = yPos;
}

function radians(degrees) {
	return degrees * Math.PI / 180;
}